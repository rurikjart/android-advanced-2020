package com.rurik.songs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SongDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_detail_activity);

        if (savedInstanceState == null) {
            String songDetails = getIntent().getStringExtra(Song.SONG_DETAILS);
            SongDetailFragment fragment = SongDetailFragment.newInstance(songDetails);
            getSupportFragmentManager().beginTransaction().add(R.id.song_detail_container, fragment).commit();
        }
    }
}
