package com.rurik.songs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SongDetailFragment extends Fragment {

    public String mSongDetails;

    public SongDetailFragment() {
        //
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(Song.SONG_DETAILS)) {
            mSongDetails = getArguments().getString(Song.SONG_DETAILS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.song_detail_fragment, container, false);

        if (mSongDetails != null) {
            TextView textView = rootView.findViewById(R.id.song_detail);
            textView.setText(mSongDetails);
        }

        return rootView;
    }


    public static SongDetailFragment newInstance(String songDetails) {
        SongDetailFragment fragment = new SongDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString(Song.SONG_DETAILS, songDetails);
        fragment.setArguments(arguments);
        return fragment;
    }




}
