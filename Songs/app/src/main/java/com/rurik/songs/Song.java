package com.rurik.songs;

public class Song {

    public static final String SONG_DETAILS = "song_details";


    private final String title;
    private final String details;


    public Song(String title, String details) {
        this.title = title;
        this.details = details;
    }


    public String getTitle() {
        return title;
    }


    public String getDetails() {
        return details;
    }

}
