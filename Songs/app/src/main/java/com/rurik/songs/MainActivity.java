package com.rurik.songs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private boolean mOnePane = true;


    // реализуем слушатель
    private  SimpleItemRecyclerViewAdapter.OnSongClickListener songClickListener = new SimpleItemRecyclerViewAdapter.OnSongClickListener() {
        @Override
        public void onSongClick(String songDetails) {

            if (mOnePane = true) {
                Intent intent = new Intent(getBaseContext(), SongDetailActivity.class);
                intent.putExtra(Song.SONG_DETAILS, songDetails);
                startActivity(intent);
            } else {
                SongDetailFragment fragment = SongDetailFragment.newInstance(songDetails);
                getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.song_detail_container, fragment)
                                            .addToBackStack(null)
                                            .commit();
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_list);

        RecyclerView recyclerView = findViewById(R.id.song_list);

        //привнести более внятную компоновку в интерфейс пользователя через LayoutManager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(SongListGenerator.generateSongsList(), songClickListener));






        if (findViewById(R.id.song_detail_container) == null) {
            mOnePane = false;
        }
    }


}




