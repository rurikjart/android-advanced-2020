package com.rurik.songs;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.TextView;

import java.util.List;

public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

    private final List<Song> mValues;
    private OnSongClickListener mSongClickListener;


    interface OnSongClickListener {
        void onSongClick(String songDetails);
    }

    public SimpleItemRecyclerViewAdapter(List<Song> items, OnSongClickListener songClickListener) {
        this.mValues = items;
        this.mSongClickListener = songClickListener;
    }

    @NonNull
    @Override
    public SimpleItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleItemRecyclerViewAdapter.ViewHolder holder, int position) {
                holder.bind(position + 1, mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private  final  View mView;
        private  final TextView mIdView;
        private final TextView mContentView;

        public ViewHolder(@NonNull View view) {
            super(view);
            this.mView = view;
            this.mIdView = view.findViewById(R.id.id);
            this.mContentView = view.findViewById(R.id.content);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String details = mValues.get(getLayoutPosition()).getDetails();
                    mSongClickListener.onSongClick(details);
                }
            });
        }

        void bind(int position, Song song){
            mIdView.setText(String.valueOf(position));
            mContentView.setText(song.getTitle());
        }


    }
}
