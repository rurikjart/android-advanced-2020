package com.ut.mysqllightapplicationpactrver.dataclass;

import java.util.Objects;

public class Task {
    private long id;
    private String text;

    public Task(long id, String text) {
        this.id = id;
        this.text = text;
    }

    public Task(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", text='" + text + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                text.equals(task.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text);
    }
}
