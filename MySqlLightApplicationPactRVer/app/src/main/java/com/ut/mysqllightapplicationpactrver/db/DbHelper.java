package com.ut.mysqllightapplicationpactrver.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.ut.mysqllightapplicationpactrver.dataclass.Task;

import java.util.ArrayList;
import java.util.Collection;

public class DbHelper extends SQLiteOpenHelper {

    // если вы изменяете стурктуру базы данных, то должны поменять и её версию
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Tasks.db";


    //  наименование столбцов вытащили в операционные константы
    public static final String TASKS_TABLE = "tasks";
    public static final String ID_COLUMN = "_id";
    public static final String TASK_COLUMN = "text";

    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // создание таблиц
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TASKS_TABLE + "( "
                + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TASK_COLUMN + " TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //обновление базы данных при обновлении структуры
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // обновление базы данных при откатывании структуры на предыдущии версию
    }

    //вставка данных в БД
    public void insert(Task task){
        //достаем БД в режим записи
        SQLiteDatabase db = getWritableDatabase();

        // создаем структуру клюс значение куда ложим поля класса и готовым информацию к вставке
        ContentValues values = new ContentValues();
        values.put(TASK_COLUMN, task.getText());

        //вставляем значение, получая идентификатор созданного объекта в базе
        long newRowId = db.insert(TASKS_TABLE, null, values);
    }

    //метод извлечения данных из БД и дальнейшего извлечения массива объектов
    public Collection<Task> getTasks(String taskQuery) {
        //активируем базу данных для чтение информации
        SQLiteDatabase db = getReadableDatabase();

        // определяем перечень столбцов для запрса из БД
        String[] projection = {
                ID_COLUMN,
                TASK_COLUMN
        };

        //используется для запроса where task = 'someText'
        String selection = taskQuery != null ? TASK_COLUMN + " = ?" : null;

        // Подставляется вместо ? в строке selection
        String[] selectionArgs = taskQuery != null ? new String[]{taskQuery} : null;

        // Сортировка результатов
        String sortOrder = TASK_COLUMN + " ASC";

        //в данной задаче используем курсор
        Cursor cursor = db.query(
                TASKS_TABLE,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        //для хранения полученных посредством курсора данных из БД применяем ArrayList массив объектов
        ArrayList<Task> tasks = new ArrayList<>();

        // пробегаемся по курсору который для нас уже вытащил данные
        while (cursor.moveToNext()) {
            int idColumnIndex = cursor.getColumnIndexOrThrow(ID_COLUMN);
            long id = cursor.getLong(idColumnIndex);

            int taskColumnIndex = cursor.getColumnIndexOrThrow(TASK_COLUMN);
            String task = cursor.getString(taskColumnIndex);

            //после каждого прохода добавляем новые значения в массив объектов
            tasks.add(new Task(id, task));

        }
        cursor.close();



        return tasks;
    }

    // удаление всех записей из таблици БД
    public void deleteAllTasks(String taskQuery) {
        // формируем аргументы для стандартного метода удаления
        String selection = taskQuery != null ? TASK_COLUMN + " = ?" : null;
        String[] selectionArgs = taskQuery != null ? new String[]{taskQuery} : null;

        // базу в режим записи
        SQLiteDatabase db = getWritableDatabase();
        int deletedRows = db.delete(TASKS_TABLE, selection, selectionArgs);

    }


}
