package com.ut.mysqllightapplicationpactrver;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.ut.mysqllightapplicationpactrver.dataclass.Task;
import com.ut.mysqllightapplicationpactrver.db.DbHelper;

import java.util.Collection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //инициализация базы данных и выполнение всех операций
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DbHelper dbHelper = new DbHelper(getApplicationContext());

                // предварительно удаляем все записи из таблици БД
                dbHelper.deleteAllTasks(null);

                dbHelper.insert(new Task(0,"Выучить Sqlite"));
                dbHelper.insert(new Task(0,"Выучить Room"));
                dbHelper.insert(new Task(0,"Выучить Retrofit"));

                // делаем запрос к базе данных null указывает что будут возвращены все записи таблици
                Collection<Task> words = dbHelper.getTasks(null);

                Log.d("dc.MainActivity", words.toString());
                return null;
            }
        }.execute();



    }
}