package com.ut.testappandroidtheory;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

@SuppressLint("StaticFieldLeak")
public class PersonViewModel extends ViewModel {
    private PersonRepository personRepository;
    private MutableLiveData<String> nameLiveData;

    public PersonViewModel(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public LiveData<String> getName() {
        if (nameLiveData == null) {
            nameLiveData = new MutableLiveData<>();
            requestName();
        }
        return nameLiveData;
    }

    private void requestName() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                return personRepository.getName();
            }

            @Override
            protected void onPostExecute(String result) {
                nameLiveData.setValue(result);
            }

        }.execute();
    }

}