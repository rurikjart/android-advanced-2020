package com.ut.testappandroidtheory;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;

import com.ut.testappandroidtheory.android.os.AsyncTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PersonViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private PersonRepository stubRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getName_validResponse_returnRepositoryAnswer() {
        // arrange
        String expectedResult = "Ваня";
        Mockito.when(stubRepository.getName()).thenReturn(expectedResult);
        PersonViewModel viewModel = new PersonViewModel(stubRepository);

        // act
        LiveData<String> liveData = viewModel.getName();

        // assert
        Assert.assertEquals(expectedResult, liveData.getValue());

    }

}
