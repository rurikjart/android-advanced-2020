package com.ut.mydagger2applicationtheory;

import javax.inject.Inject;

public class PersonRepository {
    private HttpClient httpClient;

    @Inject
    public PersonRepository(HttpClient httpClient) {
        this.httpClient = httpClient;
    }




    public String getPersonName() {
        return httpClient.getPersonName();
    }
}
