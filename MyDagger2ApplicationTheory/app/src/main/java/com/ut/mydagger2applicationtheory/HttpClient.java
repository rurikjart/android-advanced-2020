package com.ut.mydagger2applicationtheory;

import javax.inject.Inject;

public class HttpClient {

    @Inject
    public HttpClient() {}

    public String getPersonName() {
        return "Иван";
    }
}
