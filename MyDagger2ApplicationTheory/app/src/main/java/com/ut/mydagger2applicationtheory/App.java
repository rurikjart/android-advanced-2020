package com.ut.mydagger2applicationtheory;

import android.app.Application;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init();
    }

}
