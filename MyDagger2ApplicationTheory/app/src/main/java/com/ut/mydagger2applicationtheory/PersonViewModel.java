package com.ut.mydagger2applicationtheory;

import android.arch.lifecycle.ViewModel;
import android.util.Log;

import javax.inject.Inject;

public class PersonViewModel extends ViewModel {


  private  PersonRepository personRepository;
     @Inject
    public PersonViewModel(PersonRepository personRepository) {
         Log.d("dc.ViewModel", "personViewModel created");
         this.personRepository = personRepository;
    }

    public String getPersonName() {
        return personRepository.getPersonName();
    }
}
