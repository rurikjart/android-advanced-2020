package com.ut.mydagger2applicationtheory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.arch.lifecycle.ViewModelProviders;


import javax.inject.Inject;

import dagger.internal.DaggerCollections;

public class MainActivity extends AppCompatActivity {

    @Inject ViewModelFactory viewModelFactory;
    private PersonViewModel personViewModel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


     //  PersonViewModel personViewModel = new PersonViewModel(new PersonRepository());
     /*   DaggerPersonComponent.create().inject(personViewModel);*/
    // PersonComponent personComponent = DaggerPersonComponent.create();
     //внедряем объект PersonViewModel
    // personComponent.inject(this);


     // внедряем объект PersonRepository в объект PersonViewModel
    // personComponent.inject(personViewModel);
    //DaggerPersonComponent.create().inject(this);


        Injector.getAppComponent().inject(this);
        personViewModel = ViewModelProviders.of(this, viewModelFactory).get(PersonViewModel.class);


        Log.d("dc.MainActivity", "personViewModel#" + personViewModel.hashCode());
        Log.d("dc.MainActivity", personViewModel.getPersonName());


    }


}