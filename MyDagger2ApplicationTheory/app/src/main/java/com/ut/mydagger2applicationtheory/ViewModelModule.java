package com.ut.mydagger2applicationtheory;

import android.arch.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @IntoMap
    @Binds
    @ViewModelKey(PersonViewModel.class)
    public abstract ViewModel personViewModel(PersonViewModel personViewModel);

    @IntoMap
    @Binds
    @ViewModelKey(PersonDetailsViewModel.class)
    public  abstract ViewModel personDetaiViewModel(PersonDetailsViewModel personDetailsViewModel);
}