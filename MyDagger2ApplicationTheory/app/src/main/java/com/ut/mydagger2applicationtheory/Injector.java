package com.ut.mydagger2applicationtheory;

public class Injector {
    private static AppComponent appComponent;

    public static void init() {
        appComponent = DaggerAppComponent.create();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
