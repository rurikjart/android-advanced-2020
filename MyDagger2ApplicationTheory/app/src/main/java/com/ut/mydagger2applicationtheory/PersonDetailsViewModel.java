package com.ut.mydagger2applicationtheory;

import android.arch.lifecycle.ViewModel;
import android.util.Log;

import javax.inject.Inject;

public class PersonDetailsViewModel extends ViewModel {

    private PersonRepository personRepository;

    @Inject
    public PersonDetailsViewModel(PersonRepository personRepository) {
        Log.d("dc.ViewModel", "personDetailsViewModel created");
        this.personRepository = personRepository;
    }

    public String getPersonDetails() {
        return "person details: " + personRepository.getPersonName();
    }
}