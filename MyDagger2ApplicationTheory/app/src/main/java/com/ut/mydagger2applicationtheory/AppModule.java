package com.ut.mydagger2applicationtheory;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class AppModule {

    @Provides
    @Singleton
    public OkHttpClient okHttpClient() {
        return  new OkHttpClient();
    }
}
