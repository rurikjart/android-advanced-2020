package com.ut.mydagger2applicationtheory;


import com.ut.mydagger2applicationtheory.MainActivity;

import dagger.Component;


@Component(modules = {AppModule.class, ViewModelModule.class})
@Singleton
public interface AppComponent {

    void inject(MainActivity mainActivity);

}
