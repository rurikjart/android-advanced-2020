package com.ut.testapppractice;

import java.io.IOException;

public class PersonRepository {
    private HttpClient httpClient;
    private AppDatabase appDatabase;


    public PersonRepository(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public PersonRepository(HttpClient httpClient, AppDatabase appDatabase) {
        this.httpClient = httpClient;
        this.appDatabase = appDatabase;
    }

    public String getName(boolean isNetworkEnabled) throws IOException {
        if(isNetworkEnabled) {
            String name = httpClient.getName();
            appDatabase.saveName(name);
            return appDatabase.getName();
        } else {
           return appDatabase.getName();
        }
    }
}