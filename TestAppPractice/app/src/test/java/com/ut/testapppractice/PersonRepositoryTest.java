package com.ut.testapppractice;

import android.util.Log;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

public class PersonRepositoryTest {

    @Mock
    private HttpClient httpClient;

    @Mock
    private AppDatabase appDatabase;


    @Before
    public  void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getName_networkEnabled_returnedValidResponse() throws IOException {
        // проверяем что возвращается ваня
        //arrange - подготавливаем объекты для работы и тестирования
        final String serverResponce = "Ваня";
        Mockito.when(httpClient.getName()).thenReturn(serverResponce);
        PersonRepository repository = new PersonRepository(httpClient);

        // act выполнение
        String result = repository.getName(true);

        // Assert проверяем утверждение
        Assert.assertEquals(serverResponce, result);

    }

    // проверяем выкенет ли исключение программа при отсутствии сети метод try/catch
    @Test(expected = IOException.class)
    public void getName_networkDisabled_throwException() throws IOException {
        //arrange
       String serverResponce = "Ваня";
        Mockito.when(httpClient.getName()).thenReturn(serverResponce);
        PersonRepository repository = new PersonRepository(httpClient);

        // act выполнение
       repository.getName(false);

        // Assert проверяем утверждение должно выкинуть исключение

    }


    // проверяем что произошло исключение стандартным способом
    @Test
    public void getName_networkDisabled_throwException_TRYCATCH() {

        //arrange
        Exception exception = null; // убеждаемся что в начале тестирования все исключения зачищены

        String serverResponce = "Ваня";
        Mockito.when(httpClient.getName()).thenReturn(serverResponce);
        PersonRepository repository = new PersonRepository(httpClient);

        // act
        try {
            repository.getName(false);
        } catch (Exception e) {
            exception = e;

        }

        //Assert проверяем есть ли в пересенной содержимое и данные об ошибке
        Assert.assertNotNull(exception);
      }


    @Test
    public void getName_networkEnabled_saveNameToDbAndReturnDbAnswer() throws IOException {
        // arrange
        String response = "Ваня";
        Mockito.when(httpClient.getName()).thenReturn(response);
        Mockito.when(appDatabase.getName()).thenReturn(response);
        PersonRepository repository = new PersonRepository(httpClient, appDatabase);

        // act
        String result = repository.getName(true);

        // assert
        Assert.assertEquals(response, result);
        Mockito.verify(appDatabase).saveName(response);
    }

    @Test
    public void getName_networkDisabled_dontSaveToDbAndReturnDbAnswer() throws IOException {
        // arrange
        String response = "Ваня";
        Mockito.when(httpClient.getName()).thenReturn(response);
        Mockito.when(appDatabase.getName()).thenReturn(response);
        PersonRepository repository = new PersonRepository(httpClient, appDatabase);

        // act
        String result = repository.getName(false);

        // assert
        Assert.assertEquals(response, result);
        Mockito.verify(appDatabase, Mockito.never()).saveName(response);
    }



}
