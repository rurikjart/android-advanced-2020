package com.ut.mysqllightapplication.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.ut.mysqllightapplication.dataclass.Word;

import java.util.ArrayList;
import java.util.Collection;

public class DbHelper extends SQLiteOpenHelper {


    // если вы изменяете стурктуру базы данных, то должны поменять и её версию
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Words.db";

 //  наименование столбцов вытащили в операционные константы
    public static final String WORDS_TABLE = "words";
    public static final String ID_COLUMN = "_id";
    public static final String WORD_COLUMN = "word";
    public static final String DEFINITION_COLUMN = "definition";


    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // создание таблиц
        db.execSQL("CREATE TABLE " + WORDS_TABLE + "( "
                + ID_COLUMN + " INTEGER PRIMARY KEY, "
                + WORD_COLUMN + " TEXT, "
                + DEFINITION_COLUMN + " TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                //обновление базы данных при обновлении структуры
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // обновление базы данных при откатывании структуры на предыдущии версию
    }

    //вставка данных в базу данных
    public void insert(Word word){
        //достаем базу данных в которую можно существить запись

        SQLiteDatabase db = getWritableDatabase();

        // создаем структуру ключ згачение, куда помещаем поля класса
        ContentValues values = new ContentValues();
        values.put(WORD_COLUMN, word.getWord());
        values.put(DEFINITION_COLUMN, word.getDefinition());

        //вставляем значение, получая идентификатор созданного объекта в базе
        long newRowId = db.insert(WORDS_TABLE, null, values);
    }

    //вытаскиваем информацию из базы в эрей лист для дальнейшего использования
    public Collection<Word> getWords(String wordQuery) {

        //активируем базу данных на чтение информации
        SQLiteDatabase db = getReadableDatabase();

        // Определяет, какие столбцы вернуть из базы. Если передаём null, то возвращаются все столбцы (аналог - *).
        String[] projection = {
                ID_COLUMN,
                WORD_COLUMN,
                DEFINITION_COLUMN
        };

        // Используется для запроса WHERE word = 'someName'
        String selection = wordQuery != null ? WORD_COLUMN + " = ?" : null;
        // Подставляется вместо ? в строке selection
        String[] selectionArgs = wordQuery != null ? new String[]{wordQuery} : null;

        // Сортировка результатов
        String sortOrder = WORD_COLUMN + " ASC";

        Cursor cursor = db.query(
                WORDS_TABLE,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        ArrayList<Word> words = new ArrayList<>();

        while (cursor.moveToNext()) {
            int idColumnIndex = cursor.getColumnIndexOrThrow(ID_COLUMN);
            long id = cursor.getLong(idColumnIndex);

            int wordColumnIndex = cursor.getColumnIndexOrThrow(WORD_COLUMN);
            String word = cursor.getString(wordColumnIndex);

            int definitionColumnIndex = cursor.getColumnIndexOrThrow(DEFINITION_COLUMN);
            String definition = cursor.getString(definitionColumnIndex);


            words.add(new Word(id, word, definition));
        }
        cursor.close();

        return words;
    }

    //удаление всех записей из таблици БД
    public void deleteAllTasks(String wordQuery){
        String selection = wordQuery != null ? WORD_COLUMN + " = ?" : null;
        String[] selectionArgs = wordQuery != null ? new String[]{wordQuery} : null;

        SQLiteDatabase db = getWritableDatabase();
        int deletedRows = db.delete(WORDS_TABLE, selection, selectionArgs);
    }

}
