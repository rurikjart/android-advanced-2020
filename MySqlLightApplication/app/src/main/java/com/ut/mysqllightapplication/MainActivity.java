package com.ut.mysqllightapplication;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.ut.mysqllightapplication.*;

import com.ut.mysqllightapplication.dataclass.Word;
import com.ut.mysqllightapplication.db.DbHelper;

import java.util.Collection;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //инициализация базы данных и выполнение всех операций
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DbHelper dbHelper = new DbHelper(getApplicationContext());

                // предварительно удаляем все записи из таблици БД
                dbHelper.deleteAllTasks(null);

                dbHelper.insert(new Word("major", "основной"));
                dbHelper.insert(new Word("payload", "полезная нагрузка"));
                dbHelper.insert(new Word("flown", "доставленный"));

                // делаем запрос к базе данных null указывает что будут возвращены все записи таблици
                Collection<Word> words = dbHelper.getWords(null);

                Log.d("dc.MainActivity", words.toString());
                return null;
            }
        }.execute();

    }
}