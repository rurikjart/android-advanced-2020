package com.ut.mysqllightapplication.dataclass;

import java.util.Objects;

public class Word {
    private long id;
    private String word;
    private String definition;

    public Word(String word, String definition) {
        this.word = word;
        this.definition = definition;
    }

    public Word(long id, String word, String definition) {
        this.id = id;
        this.word = word;
        this.definition = definition;
    }

    public long getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getDefinition() {
        return definition;
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", word='" + word + '\'' +
                ", definition='" + definition + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return id == word1.id &&
                word.equals(word1.word) &&
                definition.equals(word1.definition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, word, definition);
    }

    // методы equals, hashcode опущены для краткости
}


