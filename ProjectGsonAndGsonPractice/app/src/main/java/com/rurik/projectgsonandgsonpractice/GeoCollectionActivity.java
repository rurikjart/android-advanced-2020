package com.rurik.projectgsonandgsonpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rurik.projectgsonandgsonpractice.DataClass.GeoInfo;

import java.lang.reflect.Type;
import java.util.Collection;

public class GeoCollectionActivity extends AppCompatActivity {
    
    private static final Gson GSON = new Gson();
    
    private  static  final String RESPONCE = "[\n" +
            "    {\n" +
            "        \"geo_coordinate\": \"31, 44\",\n" +
            "        \"geo_place\": \"NY\",\n" +
            "        \"geo_notes\": \"very good place\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"geo_coordinate\": \"48, 61\",\n" +
            "        \"geo_place\": \"LA\",\n" +
            "        \"geo_notes\": \"very good place too\"\n" +
            "    }\n" +
            "]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_coll);

        Collection<GeoInfo> geoInfoCollection = getGeoInfo(RESPONCE);
        Log.d("dc.MainActivity",geoInfoCollection.toString());
    }

    private Collection<GeoInfo>  getGeoInfo(String responce) {
        Type geoInfoCollectionType = new TypeToken<Collection<GeoInfo>>(){}.getType();
        return GSON.fromJson(responce, geoInfoCollectionType);
    }
}
