package com.rurik.projectgsonandgsonpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rurik.projectgsonandgsonpractice.DataClass.Person;

import java.lang.reflect.Type;
import java.util.Collection;

public class PersonCollectionActivity extends AppCompatActivity {

    private final static String JSON = "[  \n" +
            "  {  \n" +
            "    \"perston_id\": 1,  \n" +
            "  \"perston_name\": \"Иван\",  \n" +
            "  \"perston_age\": 31,  \n" +
            "  \"person_city\": \"Минск\"  \n" +
            "  },  \n" +
            "  \n" +
            "  {  \n" +
            "    \"perston_id\": 2,  \n" +
            "  \"perston_name\": \"Михаил\",  \n" +
            "  \"perston_age\": 35,  \n" +
            "  \"person_city\": \"Киев\"  \n" +
            "  },  \n" +
            "  \n" +
            "  {  \n" +
            "    \"perston_id\": 3,  \n" +
            "  \"perston_name\": \"Виталий\",  \n" +
            "  \"perston_age\": 40,  \n" +
            "  \"person_city\": \"Москва\"  \n" +
            "  }  \n" +
            "]";

    private static final Gson GSON = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_coll);

        Type PersonCollectionType = new TypeToken<Collection<Person>>(){}.getType();
        Collection<Person> people = GSON.fromJson(JSON, PersonCollectionType);
        Log.d("dc.MainActivity", people.toString());

    }
}
