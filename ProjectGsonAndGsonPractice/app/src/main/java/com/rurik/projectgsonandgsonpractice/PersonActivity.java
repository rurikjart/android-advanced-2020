package com.rurik.projectgsonandgsonpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.rurik.projectgsonandgsonpractice.DataClass.Person;

public class PersonActivity extends AppCompatActivity {

    private final static String  JSON = "{  \n" +
            "  \"id\" : 1,  \n" +
            "  \"name\": \"Иван\",  \n" +
            "  \"age\": 31,  \n" +
            "  \"city\": \"Минск\"  \n" +
            "}";

    private static final Gson GSON = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        Person person = GSON.fromJson(JSON, Person.class);
        Log.d("dc.MainActivity", person.toString());
    }
}
