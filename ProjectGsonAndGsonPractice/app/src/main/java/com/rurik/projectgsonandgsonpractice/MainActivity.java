package com.rurik.projectgsonandgsonpractice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onDefActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, DefActivity.class);
        startActivity(intent);
    }

    public void onGeoActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, GeoActivity.class);
        startActivity(intent);
    }

    public void onGeoCollectionActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, GeoCollectionActivity.class);
        startActivity(intent);
    }

    public void onPersonActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, PersonActivity.class);
        startActivity(intent);
    }
    public void onPersonCollectionActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, PersonCollectionActivity.class);
        startActivity(intent);
    }
}
