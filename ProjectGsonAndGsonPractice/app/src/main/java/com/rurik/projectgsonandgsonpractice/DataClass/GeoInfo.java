package com.rurik.projectgsonandgsonpractice.DataClass;


import com.google.gson.annotations.SerializedName;

public class GeoInfo {

    @SerializedName("geo_coordinate")
    private String coordinate;

    @SerializedName("geo_place")
    private String place;

    @SerializedName("geo_notes")
    private String notes;



    public GeoInfo(String coordinate, String place, String notes) {
        this.coordinate = coordinate;
        this.place = place;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "GeoInfo{" +
                "coordinate='" + coordinate + '\'' +
                ", place='" + place + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
