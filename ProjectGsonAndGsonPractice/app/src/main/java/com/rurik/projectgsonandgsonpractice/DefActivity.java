package com.rurik.projectgsonandgsonpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.rurik.projectgsonandgsonpractice.DataClass.GeoInfo;

import org.json.JSONException;
import org.json.JSONObject;



public class DefActivity extends AppCompatActivity {

    private static final String RESPONSE = "{\n" +
            "  \"geo_coordinate\": \"31, 44\",\n" +
            "  \"geo_place\": \"NY\",\n" +
            "  \"geo_notes\": \"notes\"\n" +
            "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_def);

        GeoInfo geoInfo = null;
        try {
            geoInfo = getGeoInfo(RESPONSE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("dc.MainActivity", geoInfo.toString());

    }

    private GeoInfo getGeoInfo(String response) throws JSONException {
        JSONObject geoInfoJson = new  JSONObject(response);
        String coordinate = geoInfoJson.getString("geo_coordinate");
        String place = geoInfoJson.getString("geo_place");
        String notes = geoInfoJson.getString("geo_notes");
        return new GeoInfo(coordinate, place, notes);
    }


}
