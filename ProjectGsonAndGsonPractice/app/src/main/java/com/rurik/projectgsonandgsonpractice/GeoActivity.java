package com.rurik.projectgsonandgsonpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.rurik.projectgsonandgsonpractice.DataClass.GeoInfo;

public class GeoActivity extends AppCompatActivity {

    private  static  final Gson GSON = new Gson();

    private static final String RESPONSE = "{\n" +
            "  \"geo_coordinate\": \"31, 44\",\n" +
            "  \"geo_place\": \"NY\",\n" +
            "  \"geo_notes\": \"notes\"\n" +
            "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);

        GeoInfo geoInfo = getGeoInfo(RESPONSE);
        Log.d("dc.MainActivity", geoInfo.toString());
   }

    private GeoInfo getGeoInfo(String response) {
        return  GSON.fromJson(response, GeoInfo.class);
    }
}
