package com.rurik.projectgsonandgsonpractice.DataClass;


import com.google.gson.annotations.SerializedName;

public class Person {
    @SerializedName("perston_id")
    private int id;

    @SerializedName("perston_age")
    private  int age;

    @SerializedName("perston_name")
    private String name;

    @SerializedName("person_city")
    private  String city;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
