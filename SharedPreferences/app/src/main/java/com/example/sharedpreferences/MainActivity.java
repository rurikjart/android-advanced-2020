package com.example.sharedpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    private static final String NIGHT_MODE_KEY = "nightMode";
    private static final String AUTO_PLAY_VIDEO_KEY = "autoPlayVideo";
    private static final String SHARED_PREF_FILE = "com.ut.sharedpreferencespractice.sharedprefs";

    private SharedPreferences mPreferences;

    private Switch nightModeSwitch;
    private Switch showTextSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nightModeSwitch = findViewById(R.id.night_mode_switch);
        showTextSwitch = findViewById(R.id.show_text_switch);

        // создаем или пересоздаем доступ файл локальных параметров если он уже есть
        mPreferences = getSharedPreferences(SHARED_PREF_FILE, MODE_PRIVATE);

        //вытаскиваем ранее созданные настройки для их дальнейшего применения
        boolean isNightMode = mPreferences.getBoolean(NIGHT_MODE_KEY, nightModeSwitch.isChecked());
        nightModeSwitch.setChecked(isNightMode);

        boolean showText = mPreferences.getBoolean(AUTO_PLAY_VIDEO_KEY, false);
        showTextSwitch.setChecked(showText);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // во время приостановки приложения мы сохраняем настройки переключателей
        // поль пользовательскогоинтерфейса

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(NIGHT_MODE_KEY, nightModeSwitch.isChecked());
        editor.putBoolean(AUTO_PLAY_VIDEO_KEY, showTextSwitch.isChecked());
        editor.apply();
    }
}
