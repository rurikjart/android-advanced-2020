package com.ut.okhttpapp.apiinterface;

import com.ut.okhttpapp.dataclass.WeatherResponse;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    String BASE_URL = "https://api.openweathermap.org/data/2.5/";


    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("q") String query, @Query("appid") String appId);

}