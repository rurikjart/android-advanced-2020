package com.ut.okhttpapp.apiinterface;

import com.ut.okhttpapp.dataclass.Person;

import java.util.Collection;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PersonsService {
    String BASE_URL = "https://api.npoint.io/";

    @GET("16aa8857bbd45f9ba8a0")
    Call<Collection<Person>> getPersons();
}