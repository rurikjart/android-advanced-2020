package com.ut.okhttpapp.dataclass;

import com.google.gson.annotations.SerializedName;

public class WeatherResponse {
    private int id;
    private String name;

    @SerializedName("main")
    private WeatherMainInfo mainInfo;

    @Override
    public String toString() {
        return "WeatherResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mainInfo=" + mainInfo +
                '}';
    }
}
