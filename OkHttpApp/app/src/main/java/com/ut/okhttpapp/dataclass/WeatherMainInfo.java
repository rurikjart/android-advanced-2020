package com.ut.okhttpapp.dataclass;

import com.google.gson.annotations.SerializedName;

public class WeatherMainInfo {

    @SerializedName("temp")
    private float temperature;

    @SerializedName("temp_min")
    private float maxTemperature;

    @SerializedName("temp_max")
    private float minTemperature;

    @Override
    public String toString() {
        return "WeatherMainInfo{" +
                "temperature='" + temperature + '\'' +
                ", maxTemperature='" + maxTemperature + '\'' +
                ", minTemperature='" + minTemperature + '\'' +
                '}';
    }

}
