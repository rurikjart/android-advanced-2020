package com.ut.okhttpapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onOkHttpActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, OkHttpActivity.class);
        startActivity(intent);
    }

    public void onRetrofitActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, RetrofitActivity.class);
        startActivity(intent);
    }

    public void onPractOkHttpActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, PractOkHttpActivity.class);
        startActivity(intent);
    }

    public void onPractRetrofitActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, PractRetrofitActivity.class);
        startActivity(intent);
    }

    public void onPractOkHttpAsTActivityClick(View v) {
        Intent intent = new Intent(MainActivity.this, PractOkHttpAsTActivity.class);
        startActivity(intent);
    }

}
