package com.ut.okhttpapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class PractOkHttpAsTActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pract_ok_http_as_t);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient client = new OkHttpClient.Builder().build();

        final Request request = new Request.Builder()
                .url("https://api.npoint.io/16aa8857bbd45f9ba8a0")
                .build();




        // Реализуем классический AsyncTask и поместим внутри его синхронный вызов из библиотеки OkHttp //В данном случае использована анонимная интерпретация реализации класса Айсинг таск

        AsyncTask<Void, Void, String>  OkHttpAsyncTask = new AsyncTask<Void, Void, String> () {
            @Override
            protected void onPreExecute() {
                // выполняется в UI потоке перед тем, как будет выполнен метод doInBackground.
                // Обычно в нём отображается диалог для отображения прогресса
            }

            @Override
            protected String doInBackground(Void... params) {
                // выполняется в фоновом ( Background ) потоке. В нём необходимо выполнять тяжёлые операции:
                // http запросы, запросы к базе данных, работу с файлами

                try {


                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();

                    return  responseData;


                } catch (IOException e) {
                    e.printStackTrace();
                    return  null;
                }
            }


            @Override
            protected void onProgressUpdate(Void... values) {
                // выполняется в UI потоке при явном вызове метода publishProgress().
                // Обычно вызывается во время выполнения doInBackground(), когда вы хотите отобразить,
                // сколько времени или процентов от общего времени осталось ждать до завершения задачи
            }

            @Override
            protected void onPostExecute(String result) {
                // выполняется в UI потоке после того, как выполнился метод doInBackground().
                // Принимает входной параметр - значение, которое было передано из метода doInBackground()
                Log.d("dc.MainActivity", result);

                Toast.makeText(PractOkHttpAsTActivity.this, result, Toast.LENGTH_LONG).show();


            }
        };






// Запускаем асинхронную задачу в лоб
        OkHttpAsyncTask.execute();

    }


}
