package com.ut.okhttpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_http);


        OkHttpClient client = new OkHttpClient();

        HttpUrl url = HttpUrl.parse("https://api.openweathermap.org/data/2.5/weather")
                .newBuilder()
                .addQueryParameter("q", "London")
                .addQueryParameter("appid", "a7315ad7851af7676a75902ed0cebf73")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();




        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Ошибочный статус ответа " + response);
                }

                final String responseData = response.body().string();

                // Вызываем код в главном потоке Обрабатываем события интерфэйс of потоки и пользовательского интерфэйс
                OkHttpActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(OkHttpActivity.this, responseData, Toast.LENGTH_LONG).show();
                    }

                });
            }
        });

    }
}
