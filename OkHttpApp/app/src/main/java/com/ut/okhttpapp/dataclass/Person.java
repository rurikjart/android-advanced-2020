package com.ut.okhttpapp.dataclass;

public class Person {
    private int id;
    private int age;
    private String name;
    private String city;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
