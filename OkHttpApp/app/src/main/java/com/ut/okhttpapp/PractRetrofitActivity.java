package com.ut.okhttpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.ut.okhttpapp.apiinterface.PersonsService;
import com.ut.okhttpapp.dataclass.Person;


import java.util.Collection;


public class PractRetrofitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pract_retrofit);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PersonsService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PersonsService personsService = retrofit.create(PersonsService.class);

        personsService.getPersons().enqueue(new Callback<Collection<Person>>() {
            @Override
            public void onResponse(Call<Collection<Person>> call, Response<Collection<Person>> response) {
                if (response.isSuccessful()) {
                    Collection<Person> people = response.body();
                    Log.d("dc.MainActivity", people.toString());
                    Toast.makeText(PractRetrofitActivity.this, people.toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Collection<Person>> call, Throwable t) {

            }
        });



    }
}
