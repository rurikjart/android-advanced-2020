package com.ut.noarchitectureapp;


import android.arch.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @IntoMap
    @Binds
    @ViewModelKey(BookListViewModel.class)
    public abstract ViewModel providesBookListViewModel(BookListViewModel bookListViewModel);


    @IntoMap
    @Binds
    @ViewModelKey(BookDetailsViewModel.class)
    public abstract ViewModel providesBookDetailsViewModel(BookDetailsViewModel bookDetailsViewModel);
}
