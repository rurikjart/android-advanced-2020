package com.ut.noarchitectureapp;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;



import com.squareup.picasso.Picasso;
import com.ut.noarchitectureapp.di.Injector;
import com.ut.noarchitectureapp.di.ViewModelFactory;

import javax.inject.Inject;

public class DetailsActivity extends AppCompatActivity {
    private TextView titleTextView;
    private TextView descriptionTextView;
    private ImageView imageView;


    public static String BOOK_ID_EXTRA = "bookId";

    @Inject ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        //дергаем для управления эоементы интерфейса пользователя
        titleTextView = findViewById(R.id.title_text_view);
        descriptionTextView = findViewById(R.id.description_text_view);
        imageView = findViewById(R.id.image_view);


        // активируем базу данных через Room
        // используем методы для работы из интерфейса BookDao
        // из модуля viewModel


        long bookId = getIntent().getLongExtra(BOOK_ID_EXTRA, -1);
        if (bookId == -1) throw new IllegalArgumentException("Необходимо передать bookId параметр");


      Injector.getAppComponent().inject(this);

        BookDetailsViewModel viewModel = ViewModelProviders.of(this, viewModelFactory).get(BookDetailsViewModel.class);

        viewModel.init(bookId);
        viewModel.getBook().observe(this, new Observer<Book>() {
            @Override
            public void onChanged(@Nullable Book book) {
                DetailsActivity.this.displayBookInfo(book);
            }
        });
    }





    private void displayBookInfo(Book book) {
        titleTextView.setText(book.getTitle());
        descriptionTextView.setText(book.getDescription());
        Picasso.get().load(book.getImageUrl()).into(imageView);
    }



}