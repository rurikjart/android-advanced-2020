package com.ut.noarchitectureapp;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import java.util.concurrent.Executor;

import javax.inject.Inject;

public class BookDetailsViewModel extends ViewModel {
    private MutableLiveData<Book> bookLiveData;
    private BookRepository bookRepository;
    private Executor executor;

    private long bookId;

    @Inject
    public BookDetailsViewModel(BookRepository bookRepository, Executor executor) {
        this.bookRepository = bookRepository;
        this.executor = executor;
    }



    public void init(long bookId) {
        this.bookId = bookId;
    }

    public LiveData<Book> getBook() {
        if(bookLiveData == null) {
            bookLiveData = new MutableLiveData<>();
            loadBook();
        }
        return bookLiveData;
    }

  //  @SuppressLint("StaticFieldLeak")
    private void loadBook() {
      /*  new AsyncTask<Void, Void, Book>() {
            @Override
            protected Book doInBackground(Void... voids) {
                return bookRepository.getBook(bookId);
            }

            @Override
            protected void onPostExecute(Book book) {
                bookLiveData.setValue(book);
            }

        }.execute(); */


        executor.execute(new Runnable() {
            @Override
            public void run() {

                Book book = bookRepository.getBook(bookId);
                bookLiveData.postValue(book);

            }
        });
    }
}
