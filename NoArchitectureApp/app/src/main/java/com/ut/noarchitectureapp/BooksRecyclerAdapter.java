package com.ut.noarchitectureapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BooksRecyclerAdapter extends RecyclerView.Adapter<BooksRecyclerAdapter.BookViewHolder> {
    //тут реализуем методы адаптера
    private List<Book> books;
    private OnBookClickListener onBookClickListener;

    public BooksRecyclerAdapter(OnBookClickListener onBookClickListener) {
        books = new ArrayList<>();
        this.onBookClickListener = onBookClickListener;
    }

    public interface OnBookClickListener {
        void onItemClick(Book book);
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.book_item_view, viewGroup, false);

        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder bookViewHolder, int position) {
        Book item = books.get(position);
        bookViewHolder.bind(item);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void setItems(Collection<Book> items) {
        if (!books.isEmpty()) books.clear();

        books.addAll(items);
        notifyDataSetChanged();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTextView;
        private ImageView imageView;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.title_text_view);
            imageView = itemView.findViewById(R.id.image_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Book book = books.get(BookViewHolder.this.getLayoutPosition());
                    onBookClickListener.onItemClick(book);
                }
            });


        }

        public void bind(final Book model) {
            titleTextView.setText(model.getTitle());
            Picasso.get()
                    .load(model.getImageUrl())
                    .into(imageView);
        }
    }
}
