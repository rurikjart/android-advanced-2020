package com.ut.noarchitectureapp;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.Executor;


import javax.inject.Inject;

public class BookListViewModel extends ViewModel {
    private MutableLiveData<List<Book>> booksLiveData;
  //  private BookRepository bookRepository = new BookRepository();
  //  @Inject BookRepository bookRepository;


    private BookRepository bookRepository;
    private Executor executor;

    @Inject
    public BookListViewModel(BookRepository bookRepository, Executor executor) {
        this.bookRepository = bookRepository;
        this.executor = executor;
    }

    public LiveData<List<Book>> getBooks() {
        if(booksLiveData == null) {
            booksLiveData = new MutableLiveData<>();
            loadBooks();
        }
        return booksLiveData;
    }

   // @SuppressLint("StaticFieldLeak")
    private void loadBooks() {
       /* new AsyncTask<Void, Void, List<Book>>() {
            @Override
            protected List<Book> doInBackground(Void... voids) {
                return bookRepository.getBooks();
            }

            @Override
            protected void onPostExecute(List<Book> books) {
                booksLiveData.setValue(books);
            }

        }.execute();*/
       executor.execute(new Runnable() {
           @Override
           public void run() {
               List<Book> books = bookRepository.getBooks();
               booksLiveData.postValue(books);
           }
       });

    }

}
