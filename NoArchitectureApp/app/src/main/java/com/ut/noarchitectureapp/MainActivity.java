package com.ut.noarchitectureapp;

import android.arch.lifecycle.Observer;

import android.content.Intent;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;



import java.util.List;



import android.arch.lifecycle.ViewModelProviders;
import  com.ut.noarchitectureapp.di.Injector;
import com.ut.noarchitectureapp.di.ViewModelFactory;


import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BooksRecyclerAdapter booksAdapter;
    @Inject
    ViewModelFactory viewModelFactory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // тут дергаем список из лейоут файла и настраиваем
        // создаем адаптер для списка
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // обрабатываем нажатие на список через анонимный класс
        booksAdapter = new BooksRecyclerAdapter(new BooksRecyclerAdapter.OnBookClickListener() {
            @Override
            public void onItemClick(Book book) {

                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra(DetailsActivity.BOOK_ID_EXTRA, book.getId());
                startActivity(intent);
            }
        });

        //устанавливаем адаптер для элемента управления recyclerView
        recyclerView.setAdapter(booksAdapter);

        Injector.getAppComponent().inject(this);

        BookListViewModel viewModel = ViewModelProviders.of(this, viewModelFactory).get(BookListViewModel.class);

         viewModel.getBooks().observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(@Nullable List<Book> books) {
                booksAdapter.setItems(books);
            }
        });

    }


}