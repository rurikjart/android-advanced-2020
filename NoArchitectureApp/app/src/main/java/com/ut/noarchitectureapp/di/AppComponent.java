package com.ut.noarchitectureapp.di;



import com.ut.noarchitectureapp.DetailsActivity;
import com.ut.noarchitectureapp.MainActivity;
import com.ut.noarchitectureapp.ViewModelModule;


import javax.inject.Singleton;



import dagger.Component;

@Component(modules = {AppModule.class, ViewModelModule.class})
@Singleton
public interface AppComponent {

    void inject(MainActivity mainActivity);
    void inject(DetailsActivity detailsActivity);
}
