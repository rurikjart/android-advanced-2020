package com.ut.noarchitectureapp;

import android.app.Application;


import com.ut.noarchitectureapp.di.Injector;


public class App extends Application {
    private static App instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Injector.init(this);

    }

}
