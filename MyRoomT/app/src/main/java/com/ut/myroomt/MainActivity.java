package com.ut.myroomt;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                AppDatabase db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class, "app-database")
                        .build();

                String firstPersonName = "Иван";
                //создаем объект животое с хозяином иван
                Pet murka = new Pet(1,"Мурка", firstPersonName);
                db.getPetsDao().insertAll(murka);

                db.getPersonDao().insertAll(
                        new Person(firstPersonName, 18, "зелёный",
                                new Address("Пушкинская", 111, 4)
                        ),

                        new Person("Роман", 21, "красный",
                                new Address("Пушкинская", 111, 5)
                        ),

                        new Person("Пётр", 45, "синий",
                                new Address("Пушкинская", 111, 6)
                        )
                );

                List<PersonWithPets> allPeople = db.getPersonDao().getAllPeople();

                Log.d("dc.MainActivity", allPeople.toString());
                return null;
            }
        }.execute();
    }
}