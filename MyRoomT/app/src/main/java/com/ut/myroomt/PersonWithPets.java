package com.ut.myroomt;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;
import java.util.Objects;

public class PersonWithPets {
    @Embedded
    public Person person;

    @Relation(parentColumn = "name", entityColumn = "personName")
    public List<Pet> pets;

    @Override
    public String toString() {
        return "PersonWithPets{" +
                "person=" + person +
                ", pets=" + pets +
                '}';
    }
    // методы equals(), hash

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonWithPets that = (PersonWithPets) o;
        return Objects.equals(person, that.person) &&
                Objects.equals(pets, that.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, pets);
    }
}