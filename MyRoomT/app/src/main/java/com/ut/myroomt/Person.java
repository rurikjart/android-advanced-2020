package com.ut.myroomt;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Person {

    @PrimaryKey
    @NonNull
    private String name;
    private int age;
    private String favoriteColor;

    //для дополнения общей таблици полями из класса Address
    @Embedded(prefix = "address_")
    private Address address;

    public Person(@NonNull String name, int age, String favoriteColor, Address address) {
        this.name = name;
        this.age = age;
        this.favoriteColor = favoriteColor;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getFavoriteColor() {
        return favoriteColor;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", favoriteColor='" + favoriteColor + '\'' +
                ", address=" + address +
                '}';
    }
}



