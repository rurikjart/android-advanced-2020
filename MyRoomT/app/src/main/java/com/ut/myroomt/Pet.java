package com.ut.myroomt;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Pet {

    @PrimaryKey
    private int id;
    private String name;
    private String personName;

    // конструктор, getter, toString() методы не отображены для краткости


    public Pet(int id, String name, String personName) {
        this.id = id;
        this.name = name;
        this.personName = personName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPersonName() {
        return personName;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", personName='" + personName + '\'' +
                '}';
    }
}
