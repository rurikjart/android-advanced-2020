package com.ut.myroomt;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

@Dao
public interface PetsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Pet... pets);
}
