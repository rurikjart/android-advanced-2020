package com.ut.roompract;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;


    @Dao
    public interface TaskDao {

        @Insert
        void insert(Task... tasks);

        @Query("SELECT * FROM task")
        List<Task> getAllTasks();

        @Query("DELETE FROM task")
        void deleteAllTasks();
    }

