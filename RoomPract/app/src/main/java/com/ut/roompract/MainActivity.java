package com.ut.roompract;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "app-database")
                        .build();
                db.taskDao().deleteAllTasks();

                db.taskDao().insert(
                        new Task(0, "Выучить Sqlite"),
                        new Task(0, "Выучить Retrofit"),
                        new Task(0, "Выучить Room"));


                List<Task> tasks = db.taskDao().getAllTasks();

                Log.d("dc.MainActivity", tasks.toString());
                return null;
            }
        }.execute();
    }
}