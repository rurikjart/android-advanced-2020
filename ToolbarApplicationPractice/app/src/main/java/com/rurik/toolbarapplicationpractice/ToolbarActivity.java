package com.rurik.toolbarapplicationpractice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ToolbarActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText queryEditText;
    private Menu menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar_activity_layout);


        //дергаем тулбар под управление
        toolbar = findViewById(R.id.toolbar);
        // дергаем под упраление строку поиска внутри тулбара
        queryEditText = toolbar.findViewById(R.id.query_edit_text);
        // программирование псевдопоиска
        queryEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(v.getContext(), queryEditText.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }

                return false;
            }
        });



        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.devcolibri);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        this.menu = menu;
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_search: setSearchMode(true);
                break;
            case R.id.action_help:
                Toast.makeText(this,R.string.help_hint, Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_settings:
                Toast.makeText(this, R.string.settings_hint, Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_clear:
                queryEditText.setText("");
                break;
        }

        return true;
    }

    private void setSearchMode(final boolean searchMode) {
        menu.findItem(R.id.action_clear).setVisible(searchMode);
        menu.findItem(R.id.action_search).setVisible(!searchMode);

        queryEditText.setVisibility(searchMode ? View.VISIBLE : View.GONE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(searchMode);
        getSupportActionBar().setDisplayShowHomeEnabled(searchMode);

        if (searchMode) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSearchMode(false);
                }
            });
        }



    }
}
