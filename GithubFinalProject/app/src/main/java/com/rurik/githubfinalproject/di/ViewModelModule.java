package com.rurik.githubfinalproject.di;

import android.arch.lifecycle.ViewModel;

import com.rurik.githubfinalproject.viewmodel.RepoDetailsViewModel;
import com.rurik.githubfinalproject.viewmodel.RepoListViewModel;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @IntoMap
    @Provides
    @ViewModelKey(RepoListViewModel.class)
    public ViewModel repoListViewModel(RepoListViewModel repoListViewModel) {
        return repoListViewModel;
    }

    @IntoMap
    @Provides
    @ViewModelKey(RepoDetailsViewModel.class)
    public ViewModel repoDetailsViewModel(RepoDetailsViewModel repoDetailsViewModel) {
        return repoDetailsViewModel;
    }
}
