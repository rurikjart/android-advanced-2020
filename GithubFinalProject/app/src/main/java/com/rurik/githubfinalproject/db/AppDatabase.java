package com.rurik.githubfinalproject.db;

import com.rurik.githubfinalproject.entity.Repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


    @Database(entities = {Repository.class}, version = 1)
    public abstract class AppDatabase extends RoomDatabase {
        public static String DB_NAME = "devcolibri_github";

        public abstract RepositoryDao repositoryDao();

}
