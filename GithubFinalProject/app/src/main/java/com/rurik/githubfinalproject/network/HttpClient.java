package com.rurik.githubfinalproject.network;


import java.util.List;

import com.rurik.githubfinalproject.entity.RepositoriesResponse;
import com.rurik.githubfinalproject.entity.Repository;


import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Response;


@Singleton
public class HttpClient {

    private  final GithubService githubService;

    @Inject
    public HttpClient(GithubService githubService) {
        this.githubService = githubService;
    }

    //возвращает запрос для одного выбранного репозитория
  /*  public Repository getRepository(String repoName, String userLogin) throws IOException {

          return  getResponse(githubService.getRepo(userLogin, repoName));

    }*/


    // возвращаем запрос для списка репозиториев
    public List<Repository> getRepositories(String query) throws IOException {
        RepositoriesResponse response = getResponse(githubService.searchRepos(query));
        return response.getRepositories();
    }


    private <T> T getResponse(Call<T> call) throws IOException {
        Response<T> response = call.execute();
        if(response.isSuccessful()) {
            return (T) response.body();
        }
        else {
            throw new IOException("Неуспешный статус ответа " + response);
        }
    }

}
