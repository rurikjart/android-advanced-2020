package com.rurik.githubfinalproject.di;

import com.rurik.githubfinalproject.fragment.RepoDetailsFragment;
import com.rurik.githubfinalproject.fragment.RepoListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ViewModelModule.class, AppModule.class})
public interface AppComponent {

    void inject(RepoListFragment repoListFragment);
    void inject(RepoDetailsFragment repoDetailsFragment);
}
