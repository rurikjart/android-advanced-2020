package com.rurik.githubfinalproject;

import android.app.Application;


import com.rurik.githubfinalproject.di.AppComponent;
import com.rurik.githubfinalproject.di.AppModule;
import com.rurik.githubfinalproject.di.DaggerAppComponent;



public class App extends Application {

        private static AppComponent appComponent;
        private static App INSTANCE;

        @Override
        public void onCreate() {
            super.onCreate();
            INSTANCE = this;


            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }

    public static AppComponent getAppComponent() {
        return appComponent;
    }



}
