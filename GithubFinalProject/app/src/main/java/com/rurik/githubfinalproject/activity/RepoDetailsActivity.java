package com.rurik.githubfinalproject.activity;


import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



import com.rurik.githubfinalproject.R;

import com.rurik.githubfinalproject.fragment.RepoDetailsFragment;





public class RepoDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_REPO_NAME = "repoName";
    public static final String EXTRA_USER_LOGIN = "userLogin";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        String repoName = getIntent().getStringExtra(EXTRA_REPO_NAME);
        String userLogin = getIntent().getStringExtra(EXTRA_USER_LOGIN);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.repo_details_fragment);

        if(fragment == null) {
         throw new NullPointerException("Fragment can't be null");
        }

        RepoDetailsFragment repoDetailsFragment = (RepoDetailsFragment) fragment;
        repoDetailsFragment.updateContent(repoName, userLogin);


    }

}