package com.rurik.githubfinalproject.githubclienttest;

import com.rurik.githubfinalproject.entity.Owner;
import com.rurik.githubfinalproject.entity.Repository;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

public class Constants {


    public static final Repository REPOSITORY = new Repository(
            1,
            "name",
            "description",
            "2019-01-01T04:02:57Z",
            "2018-11-05T04:02:57Z",
            100,
            "en",
            100,
            new Owner("login", 1, "https://sample/sample.png")
    );

    public static final List<Repository> REPOSITORIES = Collections.singletonList(REPOSITORY);
    public static Executor TEST_EXECUTOR = command -> command.run();


}
