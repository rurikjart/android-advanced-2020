package com.rurik.githubfinalproject.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.rurik.githubfinalproject.entity.Repository;
import com.rurik.githubfinalproject.repository.DataRepository;
import com.rurik.githubfinalproject.viewmodel.RepoDetailsViewModel;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import  com.rurik.githubfinalproject.githubclienttest.Constants;

import java.io.IOException;
import java.util.concurrent.Executor;

public class RepoDetailsViewModelTest {

    private static final Repository REPOSITORY = Constants.REPOSITORY;
    private static final Executor TEST_EXECUTOR = Constants.TEST_EXECUTOR;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private DataRepository dataRepository;
    private RepoDetailsViewModel viewModel;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        viewModel = new RepoDetailsViewModel(dataRepository, TEST_EXECUTOR);
        viewModel.getRepository().observeForever((repository -> {}));
    }

    @Test
    public void searchRepositories_ValidRepositoryResponse_SameValueInLiveData() throws IOException {
        // arrange
        String repoName = "Android";
        String userLogin = "Androider";
        Mockito.when(dataRepository.getRepository(repoName, userLogin)).thenReturn(REPOSITORY);

        // act
        viewModel.updateContent(repoName, userLogin);

        // assert
        Repository result = viewModel.getRepository().getValue();
        Assert.assertEquals(REPOSITORY, result);
    }

    @Test
    public void updateContent_ErrorResponse_ErrorLiveDataIsTrue() throws IOException {
        // arrange
        String repoName = "Android";
        String userLogin = "Androider";
        Mockito.when(dataRepository.getRepository(repoName, userLogin)).thenThrow(new IOException());

        // act
        viewModel.updateContent(repoName, userLogin);

        // assert
        Boolean result = viewModel.isException().getValue();
        Assert.assertEquals(true, result);
    }

}
