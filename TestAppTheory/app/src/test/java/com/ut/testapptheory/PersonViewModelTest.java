package com.ut.testapptheory;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PersonViewModelTest {

    private PersonRepository stubRepository;

    @Before
    public void init() {
        stubRepository = Mockito.mock(PersonRepository.class);
    }

    @After
    public void clearState() {
        stubRepository = null;
    }

    @Test
    public void getName_networkIsEnabled_returnRepositoryAnswer() throws Exception {
        final String repositoryAnswer = "Иван";
        PersonRepository stubRepository = Mockito.mock(PersonRepository.class);
        Mockito.when(stubRepository.getName(true)).thenReturn(repositoryAnswer);

        PersonViewModel personViewModel = new PersonViewModel(stubRepository);
        String result = personViewModel.getName(true);
        Assert.assertEquals(result, repositoryAnswer);
    }

    @Test
    public void getName_networkIsDisabled_returnNull() throws Exception {
        PersonRepository stubRepository = Mockito.mock(PersonRepository.class);
        Mockito.when(stubRepository.getName(false)).thenThrow(new Exception());

        PersonViewModel personViewModel = new PersonViewModel(stubRepository);
        String result = personViewModel.getName(false);
        Assert.assertNull(result);
    }

    @Test
    public void getName_networkDisabled_throwException() throws Exception {
        PersonRepository repository = new PersonRepository();
        Exception exception = null;
        try {
            // должен выбросить исключение
            repository.getName(false);
        } catch (Exception e) {
            exception = e;
        }

        Assert.assertNotNull(exception);
    }

    @Test(expected = Exception.class)
    public void getName_networkDisabled_throwException_New() throws Exception {
        PersonRepository repository = new PersonRepository();
        repository.getName(false);
        // исключение проверяется в аннотации @Test
    }


    // Остальной код не изменился

    @Test
    public void getName_networkIsEnabled_returnRepositoryAnswer_Strukt() throws Exception {
        // arrange
        final String repositoryAnswer = "Иван";
        Mockito.when(stubRepository.getName(true)).thenReturn(repositoryAnswer);
        PersonViewModel personViewModel = new PersonViewModel(stubRepository);

        // act
        String result = personViewModel.getName(true);

        // assert
        Assert.assertEquals(result, repositoryAnswer);
    }

    @Test
    public void getName_networkIsDisabled_returnNull_Strukt() throws Exception {
        // arrange
        Mockito.when(stubRepository.getName(false)).thenThrow(new Exception());
        PersonViewModel personViewModel = new PersonViewModel(stubRepository);

        // act
        String result = personViewModel.getName(false);

        // assert
        Assert.assertNull(result);
    }


}
