package com.ut.testapptheory;

public class PersonRepository {

    public String getName(boolean isNetworkEnabled) throws Exception {
        if(isNetworkEnabled) {
            return "Ваня";
        } else {
            throw new Exception("Нет подключения к сети");
        }
    }
}