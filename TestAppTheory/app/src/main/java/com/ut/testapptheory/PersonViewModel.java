package com.ut.testapptheory;

public class PersonViewModel {
    private PersonRepository personRepository;

    public PersonViewModel(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public String getName(boolean isNetworkEnabled) {
        try {
            return personRepository.getName(isNetworkEnabled);
        } catch (Exception e) {
            return e.toString();
        }
    }
}